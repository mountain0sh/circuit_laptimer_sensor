
#include "src/PowerManagement.h"
#include "src/CmdSrv.h"
#include "src/DataMem.h"
#include "src/drvMemEsp32Flash.h"
#include "src/BtCmdSlave.h"
#include "src/SerialCmd.h"
#include "src/LapTimerSens.h"
#include "src/Buzz.h"

/* 関数プロトタイプ */
void dispComuStatus(uint8_t pwr_state);  /* 通信状態表示 */
void countLap(void);            /* 検知時処理 */
void resetMem_button(void);     /* ボタン長押しでメモリ初期化 */
/* タスク */
void SerialTask(void *pvParameters);
void BtSerialTask(void *pvParameters);



#define PIN_MAG_DETECT 16
#define PIN_SW_IO0 0            /* オプションスイッチ(未使用) */
//#define PIN_SW_PWR 14           /* 電源スイッチ */
//#define PIN_KEEP_ON 27          /* 自己電源 */
#define PIN_COMU_STATUS_LED 4   /* 通信状態表示 正論理 */
#define PIN_BUZZ 22             /* ブザー */
//#define PIN_BATT 33             /* バッテリ電圧 */

 
TaskHandle_t taskHandle[2];
DrvMemEsp32Flash drv_mem_flash;


void setup() {

  pinMode(PIN_MAG_DETECT, INPUT);
  pinMode(PIN_SW_IO0, INPUT);
  pinMode(PIN_SW_PWR, INPUT);
  pinMode(PIN_COMU_STATUS_LED, OUTPUT);
  pinMode(PIN_BUZZ, OUTPUT);
  

  power_management.init();
  
  DataMem::init(&drv_mem_flash);
  /* 初期化チェック */
  uint8_t inited = 0;
  DataMem::read(MEM_INITIALIZED, &inited);
  if(1!=inited){
    DataMem::clearAll();
  }

  buzz.init(PIN_BUZZ);

  /* BTコマンド初期化 */
  bt_cmd.init();
  /* シリアルコマンド初期化 */
  serial_cmd.init();

  /* 割り込み設定 */  
  attachInterrupt(digitalPinToInterrupt(PIN_MAG_DETECT), countLap, RISING);
  
  /* BT処理タスク開始 */
  xTaskCreatePinnedToCore(BtSerialTask, "bttask", 8192, NULL, 1, &taskHandle[0], 0);
  xTaskCreatePinnedToCore(SerialTask, "serialtask", 8192, NULL, 2, &taskHandle[1], 0);
}

 
void loop() {
  static uint32_t tim_1ms = 0;
  static uint32_t tim_10ms = 0;
  static uint32_t tim_1000ms = 0;

  uint32_t current_tim = millis();

  /* 毎周期処理 */ 
  lap_timer_sens.task();
  
  
  /* 1msごと処理 */
  while(tim_1ms <= current_tim){

    buzz.task();
        
    tim_1ms = tim_1ms + 1;
  }
  
  /* 10msごと処理 */
  while(tim_10ms <= current_tim){
    
    power_management.ctrl();
    
    uint8_t pwr_state = power_management.getPowerState();
    dispComuStatus(pwr_state);
    resetMem_button();
    
    tim_10ms = tim_10ms + 10;
  }


  /* 1000msごと処理 */
  while(tim_1000ms <= current_tim){

    DataMem::save();  /* たまに保存 */
    
    tim_1000ms += 1000;
  }

}

/* 磁石検知(立ち上がり) */
void countLap(void){
  if(digitalRead(PIN_MAG_DETECT)){
    lap_timer_sens.detect();
  }
}




/* 通信ステータスLEDトグル */
/* 引数: 電源状態(0:オン, 1:シャットダウン) */
void dispComuStatus(uint8_t pwr_state){
  volatile static int led = 1;
  volatile static uint32_t toggle_time = 0;

  if(1 == pwr_state){   /* シャットダウン */
    led = 0;            /* 消灯 */
    toggle_time = millis() - 1; /* pwr_stateが0になったときすぐ点灯するようにするため */
  }else{
    if(bt_cmd.isConnected()){ /* 接続OK */
      led = 1;          /* 点灯 */
    }else{                    /* 未接続 */
                        /* 点滅 */
      if(toggle_time < millis()){
        led = !led;
        toggle_time += 500;
      }
    }
  }
  digitalWrite(PIN_COMU_STATUS_LED, led);
}


/* 起動からボタン長押しで初期化 */
void resetMem_button(void){
  uint32_t current_time = millis();
  bool pwr_sw = digitalRead(PIN_SW_PWR);  /* 正論理 */
  static uint32_t push_time = 0;
  #define BTN_INIT_TIME (10000)           /* ボタン長押し時間 */

  if(0==push_time){ /* 初期化 */
    push_time = current_time;
  }
  if(!pwr_sw){
    push_time = current_time;
  }
  if(push_time + BTN_INIT_TIME < current_time){
    DataMem::clearAll();
    push_time = current_time;
    buzz.sound(Buzz::NOTE_C5, 50);
  }
  
}

/* シリアル通信タスク */
void SerialTask(void *pvParameters){
  while(1){
    serial_cmd.task();

    vTaskDelay(20);
  }
}

/* BTシリアル通信タスク */
void BtSerialTask(void *pvParameters){
  static bool iscon = false;
  bool iscon_cur;
  
  while(1){
    bt_cmd.task();

    iscon_cur = bt_cmd.isConnected();
    if(iscon_cur != iscon){
      if(iscon_cur){  /* 繋がった */
        buzz.sound(Buzz::NOTE_C4, 100);
      }else{    /* 途切れた */
        buzz.sound(Buzz::NOTE_C4, 100);
        buzz.sound(Buzz::NOTE_A3, 190);
      }
    }
    iscon = iscon_cur;
    
    vTaskDelay(20);
  }
}
