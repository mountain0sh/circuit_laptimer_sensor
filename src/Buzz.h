/*******************************************************************************

*******************************************************************************/

#ifndef BUZZ_H
#define BUZZ_H

#include <stdint.h>

#define BUZZ_DURATION_DEFAULT 200

class Buzz{
public:

/* 音階 */
    enum NOTE{
        NOTE_NONE = 0,
        NOTE_A3 = 69,
        NOTE_AS3,
        NOTE_B3,
        NOTE_C4,
        NOTE_CS4,
        NOTE_D4,
        NOTE_DS4,
        NOTE_E4,
        NOTE_F4,
        NOTE_FS4,
        NOTE_G4,
        NOTE_GS4,
        NOTE_A4,
        NOTE_AS4,
        NOTE_B4,
        NOTE_C5,
        NOTE_CS5,
        NOTE_D5,
        NOTE_DS5,
        NOTE_E5,
        NOTE_F5,
        NOTE_FS5,
        NOTE_G5,
        NOTE_GS5,
        NOTE_MAX
    };

    /* 音再生データ */
    typedef struct{
        NOTE note;
        uint16_t duration;
    } TONE;

    Buzz(void){_pin = 0xFF;};   /*  */
    void init(uint8_t pin);
    void task(void);        /* 1ms */

    bool sound(NOTE note, uint16_t duration = BUZZ_DURATION_DEFAULT);
    bool sound(TONE* notes, uint8_t data_len);
    bool soundDouble(NOTE note0, NOTE note1);

private:
    uint8_t _pin;

    /* 音再生データバッファ */
    #define TONE_BUFF_NUM 10
    
    TONE tone_buff[TONE_BUFF_NUM];
    uint8_t write_p;
    uint8_t read_p;

    bool pushNoteBuff(TONE* note);
    bool popNoteBuff(TONE* note);
    bool readNoteBuff(TONE* note);

    /* IO */
    void driveTone(uint16_t freq);
};

extern Buzz buzz;

#endif //BUZZ_H
