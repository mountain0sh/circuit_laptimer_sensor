/*******************************************************************************

*******************************************************************************/

#ifndef POWERMANAGEMENT_H
#define POWERMANAGEMENT_H

#include <stdint.h>
#include "CmdSrvDl.h"

#define PIN_SW_PWR 14           /* 電源スイッチ */
#define PIN_KEEP_ON 27          /* 自己電源 */
#define PIN_BATT 33             /* バッテリ電圧 */

class PowerManagement : public CmdProc{
public:
    void init();
    void ctrl();
    uint8_t getPowerState();
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
private:
    uint8_t PowerState;
    uint16_t BattVoltage;
};


extern PowerManagement power_management;

#endif //POWERMANAGEMENT_H
