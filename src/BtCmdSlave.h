/*******************************************************************************

*******************************************************************************/

#ifndef BTCMD_H
#define BTCMD_H

#include <stdint.h>
#include "BluetoothSerial.h"

#include "CmdSrv.h"

class BtCmd:public CmdSrv{
public:
    BtCmd(void):connected(false),SerialBT(){};

    uint8_t init(void);
    void task(void);

    bool isConnected(void){return connected;};
    
private:
	static uint8_t my_mac[6];
	static char my_name[20];
    uint8_t connected;
    BluetoothSerial SerialBT;
    
    /* BTシリアル送信 */
    uint8_t write(uint8_t* send_msg, uint8_t send_len);

    /* コールバック */    
    /* BT接続関係 */
    static void esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param);
};

extern BtCmd bt_cmd;

#endif //BTCMD_H
