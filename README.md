# circuit_laptimer_sensor

サーキットの各セクションに埋まっている磁石を検知してラップタイムを計ります。  
このリポジトリはセンサ側です。磁気(の変化)を検知して、前回変化時からの時間を親機へ送信します。

親機はこれら。M5StackだったりAndroid端末だったり。
* https://gitlab.com/conpe_/laptimerc
* https://gitlab.com/conpe_/circuit_laptimer_disp

## 使用方法
### 充電
USB接続によりバッテリーを充電します。
赤LEDが消灯すると充電完了。\
バッテリー持続時間は1時間強。

### 電源
ボタン長押しで電源ON。再度長押しでOFF。

### 接続
電源を入れるとステータスLED(緑)が点滅し、
親機との接続が完了すると点灯に変わります。

## ソフト
## 開発環境
ESP32をArduinoで。

## 基板
doc/schフォルダに回路図と実体配線図。  
タカチのWC72-Nのケースに合うサイズです。

### 開発環境
* EAGLE 5.11.0

### 基板エラッタ・改良案
* FT234XDのRESET接続忘れ
* FT234XDのVCCIO供給が電源スイッチ経由なので面倒。電源ボタン押しながらUSB接続しないとソフト書き込み不可。\
暫定対応として、VCC2からの供給をパターンカット。3V3OUTをVCCIOとRESETに接続。3V3OUTにはキャパシタ要。
* USB端子にキャップ取り付けられるよう、端子の位置を調整したい。
* 磁気センサのパッドが無駄に長い